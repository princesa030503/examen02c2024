import express from "express";
import { fileURLToPath } from "url"
import json from "body-parser";
import path from "path";
import routerApp from "./router/index.js";



const puerto = 80;
const __filename = fileURLToPath(import.meta.url);
const __dirname = path.dirname(__filename);

const main = express();

main.set("view engine", "ejs");
main.set(express.static(__dirname + '/public'));
main.use(json.urlencoded({ extendeds: true }));
main.use(routerApp);
// main.use(misRutas.router);


main.listen(puerto, () => {
    console.log(`Servidor corriendo en http://localhost:${puerto}`);
})
