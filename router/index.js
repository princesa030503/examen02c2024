import { Router } from "express";
const router = Router();

const calcularPago = (nivel, pagoBase, horasImpartidas) => {
    let incremento = 0;
    switch (nivel) {
        case '1':
            incremento = 0.3;
            break;
        case '2':
            incremento = 0.5;
            break;
        case '3':
            incremento = 1.0;
            break;
        default:
            incremento = 0;
            break;
    }
    const pagoBaseIncrementado = pagoBase * (1 + incremento);
    return pagoBaseIncrementado * horasImpartidas;
};

const calcularImpuesto = (pagoTotal) => {
    return pagoTotal * 0.16;
};

const calcularBono = (cantidadHijos) => {
    let bono;

    if (canthijos === 0) {
        bono = 0;
    } else if (canthijos === 1 || canthijos === 2) {
        bono = pagoTotal * 0.05;
    } else if (canthijos > 2 && canthijos < 6) {
        bono = pagoTotal * 0.1;
    } else {
        bono = pagoTotal * 0.2;
    }
    
  
      pagoTotal = pagoTotal + bono;
  
};

router.post('/', (req, res) => {
    const { numDocente, nombre, domicilio, nivel, pagoBase, horasImpartidas, cantidadHijos } = req.body;

    const pagoBaseTotal = calcularPago(nivel, pagoBase, horasImpartidas);
    const impuesto = calcularImpuesto(pagoBaseTotal);
    const bono = calcularBono(cantidadHijos);
    const bonoTotal = pagoBaseTotal * bono;
    const totalAPagar = pagoBaseTotal + bonoTotal - impuesto;

    const params = {
        numDocente: numDocente,
        nombre: nombre,
        domicilio: domicilio,
        nivel: nivel,
        pagoBase: pagoBase,
        horasImpartidas: horasImpartidas,
        cantidadHijos: cantidadHijos,
        pagoBaseTotal: pagoBaseTotal,
        impuesto: impuesto,
        bonoTotal: bonoTotal,
        totalAPagar: totalAPagar
    };

    res.render('index', params);
});

router.get('/', (req, res) => {
    const { numDocente, nombre, domicilio, nivel, pagoBase, horasImpartidas, cantidadHijos } = req.query;

    const pagoBaseTotal = calcularPago(nivel, pagoBase, horasImpartidas);
    const impuesto = calcularImpuesto(pagoBaseTotal);
    const bono = calcularBono(cantidadHijos);
    const bonoTotal = pagoBaseTotal * bono;
    const totalAPagar = pagoBaseTotal + bonoTotal - impuesto;

    const params = {
        numDocente: numDocente,
        nombre: nombre,
        domicilio: domicilio,
        nivel: nivel,
        pagoBase: pagoBase,
        horasImpartidas: horasImpartidas,
        cantidadHijos: cantidadHijos,
        pagoBaseTotal: pagoBaseTotal,
        impuesto: impuesto,
        bonoTotal: bonoTotal,
        totalAPagar: totalAPagar
    };

    res.render('index', params);
});

export default router;
